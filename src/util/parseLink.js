import qs from 'querystring'
import url from 'url'
import xtend from 'xtend'

export const parseLink = (linkHeader) => {
  function hasRel(x) {
    return x && x.rel
  }

  function intoRels(acc, x) {
    function splitRel(rel) {
      acc[rel] = xtend(x, {rel: rel})
    }

    x.rel.split(/\s+/).forEach(splitRel)

    return acc
  }

  function createObjects(acc, p) {
    // rel="next" => 1: rel 2: next
    let m = p.match(/\s*(.+)\s*=\s*"?([^"]+)"?/)
    if (m) acc[m[1]] = m[2]
    return acc
  }

  function parseLink(link) {
    try {
      let m = link.match(/<?([^>]*)>(.*)/),
        linkUrl = m[1],
        parts = m[2].split(';'),
        parsedUrl = url.parse(linkUrl),
        qry = qs.parse(parsedUrl.query)

      parts.shift()

      let info = parts.reduce(createObjects, {})

      info = xtend(qry, info)
      info.url = linkUrl
      return info
    } catch (e) {
      return null
    }
  }

  if (!linkHeader) return null

  return linkHeader.split(/,\s*</)
    .map(parseLink)
    .filter(hasRel)
    .reduce(intoRels, {})
}
