import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import VueShowdown from 'vue-showdown'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// import moment from 'moment'
// import VueMomentJS from 'vue-momentjs'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueShowdown)
// Vue.use(VueMomentJS, moment)

new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app')
