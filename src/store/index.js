import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import {parseLink} from "../util/parseLink";

Vue.use(Vuex)

const endpoints = process.env.VUE_APP_API_ENDPOINT;

export default new Vuex.Store({
  state: {
    userName: process.env.VUE_APP_USER_NAME,
    repoName: process.env.VUE_APP_REPO_NAME,
    sort: 'comments',
    order: 'desc',
    nextUrl: null,
    issueList: [],
    issue: {},
  },
  getters: {
    getIssueList: state => state.issueList,
    getIssue: state => state.issue,
    getRepoName: state => `${state.userName}/${state.repoName}`
  },
  mutations: {
    updateIssueList(state, payload) {
      state.issueList.push(...payload);
    },
    updateIssueListNextUrl(state, payload) {
      state.nextUrl = payload
    },
    updateIssue(state, payload) {
      state.issue = payload
    }
  },
  actions: {
    fetchIssueList({commit}) {
      // sort=${this.state.sort}&order=${this.state.order}
      axios.get(this.state.nextUrl ? this.state.nextUrl : `${endpoints}/repos/${this.state.userName}/${this.state.repoName}/issues`, {
        params: {
          sort: this.state.sort,
          order: this.state.order
        }
      })
        .then(({data, headers}) => {
          commit('updateIssueList', data)
          commit('updateIssueListNextUrl', parseLink(headers.link).next.url)
        })
        .catch(({error}) => console.log(error))
    },
    fetchIssue({commit}, {id}) {
      axios.get(`${endpoints}/repos/${this.state.userName}/${this.state.repoName}/issues/${id}`)
        .then(({data}) => commit('updateIssue', data))
    },
  }
})
